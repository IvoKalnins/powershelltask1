# Create a PowerShell script with the switch parameter. If the switch parameter is true, then execute Get-Disk cmdlet.
Switch (3)
{
    1 {"This is text"}
    2 {notepad.exe}
    3 {Get-Disk}
}