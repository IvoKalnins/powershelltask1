# Create an object and fill it with values.

$obj10 = new-object -typename psobject
$obj10 | add-member -type noteproperty -name Name -value Ivo
$obj10 | add-member -type noteproperty -name Age -value 29
$obj10 | add-member -type noteproperty -name HairColor -value brown