# Create a PowerShell script to update the “SortAs” value to “OMPL” in the 
# JSON file. Check the file to make sure the value is updated. (1/1 done)

$path = "C:\Users\ivo.kalnins\Azure_Task1\Task1.json"
$RawText = Get-Content -Path $path -Raw
$RawText -replace "OMPL","SortAs" | Set-Content $path
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ConvertFrom-Json
# ConvertTo-Json