# Create PSM that contains one simple function with parameters. Import this module with 
# PowerShell script and execute the function (which defined in PSM) by passing the parameter

Function Add-Numbers($one, $two, $three) {
    $one + $two + $three
}

#now do the following
# 1) write in console: Import-Module .\Task1_8.psm1
# 2) pass parameters: Add-Numbers -one 1 -two 2 -three 3