1.Basic commands
https://www.youtube.com/watch?v=IHrGresKu2w
(use Tab-complete)
(use up button to get stuff from history)

Start-Transcript = enables history or sth // puts in Documents folder
Get-Command = all available in PowerShell
Get-Command -noun S* = filters from (verb-noun) all nouns that start with S+ sometext

Get-Service = displays status, name and displayname of these objects
Get-Help Get-Service = what can I do with Get-Service Cmdlet
get-help Get-Service -Examples = examples of commandget-help Get-Service -Online
get-help Get-Service -Online = open online help

cls = cleans screen. Alias Command
Get-Alias cls = outputs the Cmdlet equivalent, being Clear-Host
Get-Alias = displays all Alias commands

Get-Process = shows all running processes
get-process -name MicrosoftEdge = gets one process by name

| = take output of this command and send objects to the other side
get-process -name MicrosoftEdge | get-member = shows all properties and methods associated with specific object
get-process -name MicrosoftEdge | select-object * = returns all properties of this object

$zebra = get-process MicrosoftEdge = variable, PS saves this object to this variable
$zebra = check if variable works
$zebra.Name = tab through properties and methods

get-history = shows all my commands run during this session

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
2.https://www.youtube.com/watch?v=f9xPJXslVWE
Object manipulation

Get-PSDrive | ?{$_.Free -gt 1} | %{$Count = 0; Write-Host "";} { $_.Name + ": Used: " + "{0:N2}" -f ($_.Used/1gb) + " Free: " + "{0:N2}" -f ($_.free/1gb) + " Total: " + "{0:N2}" -f (($_.Used/1gb)+($_.Free/1gb)); $Count = $Count + $_.Free;}{Write-Host"";Write-Host "Total Free Space " ("{0:N2}" -f ($Count/1gb)) -backgroundcolor magenta}

Get-PSDrive = returns all drives associated with system
| ? >> get-alias ? = where object

Get-PSDrive | where-object {$_.Free -gt 1}
$_ = current object in the pipe
.Free -gt 1 = where free space of the object is greater than "1" (one)

| Select-Object Root, Used, Free = for objects returned by pipe, now show only property "Root, Used, Free"
Select-Object * = returns all properties available


| ForEach-Object {"Zebra"} = will cycle through items in pipe, do action in bracket/loops. prints 'Zebra'
ForEach-Object {Write-Host "Free space for" $_.root "is" $_.Free -ForegroundColor red} = write words and $_ for the variables assigned properties
($_.Free/1024/1024/1024) = lazy way to translate information from Bites to Kilobites > Megabites > Gigabites
($_.Free/1gb) = equivalent for above

"{0:N0}" -f 10000000000 = .Net function, for first item (in array?) what type of format I want to do (number) with zero decimals, -f formats
("{0:N2}" -f ($_.Free/1gb)) = update from above
; = simply separates different/done functions one from another

Get-PSDrive | where-object {$_.free -gt 1} | ForEach-Object {$c = 0; Write-Host "This step only runs once."}{$c = $c + 1; Write-Host "This section runs once for each object in the pipe." $c}{Write-Host "This step runs once everything is done. The value of the c variable is" $c}
This step only runs once.
This section runs once for each object in the pipe. 1
This section runs once for each object in the pipe. 2
This section runs once for each object in the pipe. 3
This step runs once everything is done. The value of the c variable is 3

Get-Volume = equivalent for original function

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
3. PowerShell with SharePoint
https://www.youtube.com/watch?v=zKeaGh5Z7Po
(I need sharepoint for this)

Start-Transcript

get-command -noun SP* = powershell Cmdlet
(get-command -noun SP*).count = amount of Cmdlet

get-spsite = site collection
get-spsite -limit all = see all site collections
*central admin rights not included by default* = to change anything by default
get-help get-spsite -online = online help for powershell

get-spsite | get-spweb = pipe over to spweb, give list of site collections and subsites involved
*error access denied for web* = current accout needs access to DB and actions
central admin > manage web applications list > add user / full access to site collections
SQL > install account > mappings > add access to db_owner

Add-SPShellAdmin = if you have access to all, you use this command to give someone else access
Get-SPContentDatabase = get database ID from portal
Add-SPShellAdmin -UserName shanescows\bob -database *ID from previous* = now bob has access

get-spsite -Limit all | Out-File sitecollections.txt = creates in location .txt file with site collections in farm
get-spsite -Limit all | select Url, owner = produces list..
get-spsite -Limit all | Format-Table Url, owner -autosize = makes above more readable
get-spsite -Limit all | Format-Table Url, owner -autosize | Out-File sitecollections.txt = above piped out in .txt

get-command -verb out = commands for 'out'
get-command -verb export = another way to get data out

get-spsite http://portal.shanescows.com | get-member = show all methods i can do to this particular site collection (one)
get-spsite http://portal.shanescows.com | select * = gives all values for every property it can read

get-spweb http://portal.shanescows.com
$zebra = get-spweb http://portal.shanescows.com
$zebra
$zebra.Lists = all of the lists for previous variable

get-spsite
New-SPSite = creates site collection for me
Import-CSV .\TestSite.csv = made csv file with fields name, url and template
Import-CSV .\TestSite.csv | ForEach-Object{new-spsite -url ("http://portal.shanescows.com/sites/" + $_.url) -Name $_.name -Template $_.template -OwnerAlias shanescows\bob} = looping concept to ??
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
4. https://www.youtube.com/watch?v=3Q0jG1Doa-s
PowerShell with Azure

Start-Transcript = nice history file >D

Get-ExecutionPolicy = WIN10 by default
Set-ExecutionPolicy RemoteSigned 

Install-Module azureRM = get from internet Azure resource manager Cmdlet
Install-Module azure -AllowClobber = to avoid azureRM (new portal experience) and azure(service manager classic) not to conflict processes

*goes online* to portal.azure.com
VM classic - use azure PS CMDlet to control
VM regular - use azureRM to do things

get-command -Module azurerm = gives Cmdlet, CTRL+C to cancel upload)
(get-command -Module azurerm).count

Login-AzureRmAccount = via popup box log in, shows all IDs / subscription
get-azurermvm = shows my VMs
get-azurermvm | select name = shows names of my VMs

get-azurermvm -ResourceGroupName proper2013 | ForEach {start-azurerm -Name $_.name -resourcegroupname proper2013} = resource group, for each object goes to Azure and starts each VM one at a time
*CTRL+C will not work until VM process at least for one is done*
get-azurermvm -ResourceGroupName proper2013 | ForEach {stop-azurerm -Name $_.name -resourcegroupname proper2013 -confirm:$false -force} = stops VMs

*runs another PowerShell window* = classic VMs
Add-AzureAccount = logs in Azure

get-azureVM = shows classic VM on account
Start-AzureVM -ServiceName VideoDemoClassicVMs -Name "*" = servicename >< resurcegroups
get-azureservice = see details, like 'servicename' for above

Stop-AzureVM -ServiceName VideoDemoClassicVMs -Name "*" -Force

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
5. Different commands
6. Assets + questions

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
