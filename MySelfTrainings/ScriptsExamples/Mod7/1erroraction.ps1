﻿$ErrorActionPreference
#dir variable:*pref*

#Get-WmiObject win32_bios -ComputerName DC,NotOnline,Client
Get-WmiObject win32_bios -ComputerName localhost,NotOnline

#Get-WmiObject win32_bios -ComputerName DC,NotOnline,Client -EA SilentlyContinue -EV MyError
Get-WmiObject win32_bios -ComputerName localhost,NotOnline -EA SilentlyContinue -EV MyError
$MyError

#Get-WmiObject win32_bios -ComputerName DC,NotOnline,Client -EA Stop
Get-WmiObject win32_bios -ComputerName localhost,NotOnline -EA Stop

#Get-WmiObject win32_bios -ComputerName DC,NotOnline,Client -EA Inquire
Get-WmiObject win32_bios -ComputerName localhost,NotOnline -EA Inquire
