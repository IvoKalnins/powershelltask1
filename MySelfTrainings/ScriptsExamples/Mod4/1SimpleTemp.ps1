﻿<#
 Comment based help
#>
Function Verb-Noun {

    [CmdletBinding()]
    Param(
        [Parameter()][String]$MyString,
        [Parameter()][Int]$MyInt
    )

    Begin{<#Code#> "begin $MyString"}
    Process{<#Code#> "process $MyString"}
    End{<#Code#> "end $MyString"}
}
#++++++++++++++++++++++++++++++++++++++++++++++
<#
 Comment based help
#>
Function Verb-Noun {

    [CmdletBinding()]
    Param(
        [Parameter(valuefrompipeline=$true)]
        [int]$MyInt
    )

    Begin{<#Code#> $total = 0} #starts the process
    Process{<#Code#> $total += $MyInt} #does stuff once for every item in pipeline
    End{<#Code#> "total = $total"} #ends process and shows end result
}
# 1..3 | Verb-Noun 