﻿Function set-stuff{
    [cmdletbinding(SupportsShouldProcess=$true, #this line is mandatory
                    confirmImpact='Medium')]
    param(
        [Parameter(Mandatory=$True)]
        [string]$computername   
    )
    Process{
    
        If ($psCmdlet.shouldProcess("$Computername")){
            Write-Output 'Im changing something right now'
        }
    }
}
#get-service -name bits
#get-service -name bits | stop-service
#start-service -name bits
#$ConfirmPreference = 'medium'
#get-service -name bits | stop-service >> confirmation notification pop-up

#set-stuff -whatif
#set-stuff -whatif -confirm -computername localhost
#set-stuff -verbose

#pscmdlet.ShouldProcess