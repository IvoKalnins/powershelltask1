# In PowerShell script use a loop to iterate through JSON file and print 
# out to console all keys and values equal to a defined parameter

#process these three lines
$path = ".\Task2.json"
$raw = Get-Content $path -raw
$obj = ConvertFrom-Json $raw

#notepad $path >> for file visual

#pass following parameter in console to get requested value/lines; where "dev" can be any value of your choice
$obj -match "uat"
