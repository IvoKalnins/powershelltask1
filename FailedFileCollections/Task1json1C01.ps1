# Create a PowerShell script to update the “SortAs” value to “OMPL” in the 
# JSON file. Check the file to make sure the value is updated. (0/1 done)


# $path = ".\testjson.js"
# $raw = Get-Content $path -raw

# $obj = ConvertFrom-Json $raw
# $obj.Age = 45     # I always lie about my age!

# Write-Host $obj   # Dump obj to console

# Set-Content $path $obj
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# $a = Get-Content 'D:\temp\mytest.json' -raw | ConvertFrom-Json
# $a.update | % {if($_.name -eq 'test1'){$_.version=3.0}}
# $a | ConvertTo-Json -depth 32| set-content 'D:\temp\mytestBis.json'
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# $pathJson1 = "C:\Users\ivo.kalnins\Azure_Task1\Task1.json"

# $myFile = Get-Content $pathJson1 -raw | ConvertFrom-Json
# $myFile.update | ForEach-Object {if($_.ID -eq 'SMGL'){$_.SortAs=3.33}}
# $myFile | ConvertTo-Json -depth 32| set-content "C:\Users\ivo.kalnins\Azure_Task1\Json1\Task1Upd1.json"
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$path = "C:\Users\ivo.kalnins\Azure_Task1\Task1.json"
$RawText = Get-Content -Path $path -Raw
$RawText -replace "OMPL","SortAs" | Set-Content $path