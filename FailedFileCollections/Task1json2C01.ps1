# In PowerShell script use a loop to iterate through JSON file and print 
# out to console all keys and values equal to a defined parameter

# # this is where the XML sample file was saved:
# $Path = "$env:temp\inventory.xml"
 
# # load it into an XML object:
# $xml = New-Object -TypeName XML
# $xml.Load($Path)
# # note: if your XML is malformed, you will get an exception here
# # always make sure your node names do not contain spaces
 
# # simply traverse the nodes and select the information you want:
# $Xml.Machines.Machine | Select-Object -Property Name, IP
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# $path = "C:\Users\ivo.kalnins\Azure_Task1\Task2.json"
# $obj = Get-Content $path -raw | ConvertFrom-Json

# # for($x=1; $x -le 1; $x++)
# # {
# #    $obj | Select-Object -Property RgName, Name, AppName
# # }

# $obj | ForEach-Object
# {
#     if($_.RgName -eq 'DOTcom-dev-rg')
#     {
#         "Select-Object -Property RgName, Name, AppName"
#     }
# }

# # Get-Content Task2.json
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# $path = "C:\Users\ivo.kalnins\Azure_Task1\Task2.json"
# notepad $path
# $obj = Get-Content $path -raw | ConvertFrom-Json
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# md jsontests
# new-item "testjson.json"
# notepad "testjson.json"
# { 
#     Name: "Ivo Kalnins",
#     Age: "29",
#     Gender: "Male" 
# }
# Get-Content .\testjson.json
# Get-Content "testjson.json" | ConvertFrom-Json
# # ----------------
# $path = ".\testjson.json"
# $raw = Get-Content $path -raw

# $obj = ConvertFrom-Json $raw
# $obj.Age = 45     # I always lie about my age!

# Write-Host $obj   # Dump obj to console

# Set-Content $path $obj
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$path = ".\Task2.json"
# $path
$raw = Get-Content $path -raw
# $raw
$obj = ConvertFrom-Json $raw
# $obj
# foreach($o in $obj){
#     Write-Host "new line" + $o
# }