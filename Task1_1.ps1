# Create a PowerShell script that will create 10 copies of the XML file (2/2 done)
# with a different Artist and Title (the value of the th tag) (1/1 done)

$xmlOriginal = "C:\Users\ivo.kalnins\Azure_Task1\Task1.xml"
$xmlUpdate = "C:\Users\ivo.kalnins\Azure_Task1\Copy10x"

for($i = 1; $i -le 10; $i++)
{
    Copy-Item $xmlOriginal -Destination ($xmlNew = ($xmlUpdate + "\Task1.Copy" + $i + ".xml"))
    (Get-Content $xmlNew).Replace("Title", "Title" + $i) | Set-Content $xmlNew
    (Get-Content $xmlNew).Replace("Artist", "Artist" + $i) | Set-Content $xmlNew
}

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# (0/1 as not referencing value of th tag)
# https://www.powershellmagazine.com/2013/08/19/mastering-everyday-xml-tasks-in-powershell/ >> check replace options
# https://stackoverflow.com/questions/16428559/powershell-script-to-update-xml-file-content
# https://social.technet.microsoft.com/Forums/scriptcenter/de-DE/286b0626-d1ea-48d3-91ab-9129a258e40a/how-can-i-replace-text-in-xml-node-with-powershell?forum=ITCG

# $xml = New-Object xml
# $xml.GetType()
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# $xmlOriginal = "C:\Users\ivo.kalnins\Azure_Task1\Task1.xml"
# # $xmlUpdate = "C:\Users\ivo.kalnins\Azure_Task1\Copy10x.xml"

# $xml = New-Object xml
# $xml.Load($xmlOriginal)
# # $Xml.Machines.Machine | Select-Object -Property Name, IP

# foreach($item in (Select-XML -Xml $xml -XPath '//My CD Collection'))
# {
#     if ($item.node.th -match 'Title')
#     {
#       $item.node.th = 'TitleNew'
#     }
# }
# # $xml.Save($xmlUpdate)
# # notepad $xmlUpdate