# Create PSM that contains one simple function with parameters. Import this module with 
# PowerShell script and execute the function (which defined in PSM) by passing the parameter

Function Add-Numbers($one, $two, $three) {
    $one + $two + $three
}